import * as Keychain from 'react-native-keychain';

export default class KeychainHelper {
  static setCredentials = async (username: any, password: any) => {
    return new Promise((resolve, reject) => {
      // Store the credentials
      Keychain.setGenericPassword(username, password)
        .then(resp => {
          resolve(true);
        })
        .catch(err => {
          console.log('err: ', err);
          reject(err);
        });
    });
  };

  static getCredentials = async () => {
    return new Promise((resolve, reject) => {
      Keychain.getGenericPassword()
        .then((credentials) => {
          if (credentials && credentials.username) {
            // console.log('Credentials successfully loaded for user ' + credentials.username);
            resolve(credentials);
          } else {
            // console.log('No credentials stored');
            resolve(null);
          }
        })
        .catch(err => {
          console.log('err: ', err);
          reject(err);
        });
    });
  };

  static resetCredentials = async () => {
    return new Promise((resolve, reject) => {
      Keychain.resetGenericPassword()
        .then((response) => {
          // console.log('response', response);
          resolve(response);
        })
        .catch(err => {
          console.log('err: ', err);
          reject(err);
        });
    });
  };
}
