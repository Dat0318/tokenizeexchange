import DropdownAlert from 'react-native-dropdownalert';
import {appString, getString} from 'app/presentation/theme/Theme';

export default class DropDownHolder {
  static dropDown?: DropdownAlert;

  static setDropDown (dropDown: DropdownAlert) {
    DropDownHolder.dropDown = dropDown;
  }

  static getDropDown () {
    return DropDownHolder.dropDown;
  }

  static showSuccessAlert = (message?: string, title?: string, interval?: number) => {
    if (DropDownHolder.dropDown) {
      const _title = title ? title : getString(appString.success);
      DropDownHolder.dropDown.alertWithType('success', _title, message || '', undefined, interval ? interval : 0);
    }
  };

  static showErrorAlert = (message?: string, title?: string, interval?: number) => {
    if (DropDownHolder.dropDown) {
      if (message !== "The consumer isn't authorized to access %resources.") {
        const _title = title ? title : getString(appString.error);
        DropDownHolder.dropDown.alertWithType('error', _title, message || '', undefined, interval ? interval : 0);
      }
    }
  };

  static showInfoAlert = (message?: string, title?: string, interval?: number) => {
    if (DropDownHolder.dropDown) {
      const _title = title ? title : getString(appString.info);
      DropDownHolder.dropDown.alertWithType('info', _title, message || '', undefined, interval ? interval : 0);
    }
  };
}
