import { Dimensions } from 'react-native';

const window = Dimensions.get('window');

export const getWindowWidth = () => {
  return window.width;
};

export const getWindowHeight = () => {
  return window.height;
};
