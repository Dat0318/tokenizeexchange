export const BasedCoin = {
  Bitcoin: 'BTC',
  Ethereum: 'ETH',
  Litecoin: 'LTC',
  OmiceGo: 'OMG',
  Ripple: 'XRP',
  Zcash: 'ZEC',
  Zilliqa: 'ZIL',
};

export const CommonKeys = {
  RememberMe: 'RememberMe',
};