export default class ResponseModel {
  _code = 0;
  _isError: boolean = false;
  _errorMessage: string = '';
  _data = null;

  static createSuccess(data: any) {
    const model = new ResponseModel();
    model._isError = false;
    model._data = data;
    return model;
  };

  static createError(code: number, errorMessage: string) {
    const model = new ResponseModel();
    model._isError = true;
    model._code = code;
    model._errorMessage = errorMessage;
    return model;
  }

  isSuccess = () => {
    return !this._isError;
  };
}
