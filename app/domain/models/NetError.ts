export class NetError {
  status: number = 200;
  message: string = '';
  code?: any = 0;
}