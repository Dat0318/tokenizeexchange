export interface SignInCredential {
  email: string | any;
  password: string | any;
}

export interface CustomerUseCase {
  signIn(credential: SignInCredential): Promise<any>;
}
