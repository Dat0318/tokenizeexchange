export interface HomeUseCase {
  getBaseMarkets(): Promise<any>;
  getSummaries(): Promise<any>;
  getMarkets(): Promise<any>;
}
