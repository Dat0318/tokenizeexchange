import StorageGateway from 'app/data/gateway/StorageGateway';
import { CommonKeys } from 'app/common/constant/Constants';

const getRememberMe = () => {
  return new StorageGateway().get(CommonKeys.RememberMe);
};

const saveRememberMe = (isRemember: boolean) => {
  return new StorageGateway().saveString(CommonKeys.RememberMe, isRemember.toString());
};

export const CommonRepository = {
  getRememberMe, saveRememberMe
};