import { HomeUseCase } from 'app/domain/useCases/HomeUseCase';
import { HomeResource } from 'app/data/gateway/resource';

import APIGateWay from 'app/data/gateway/APIGateway';

export class HomeRepository implements HomeUseCase {
  static create(): HomeUseCase {
    return new HomeRepository();
  }

  async getBaseMarkets(): Promise<any> {
    let api = new APIGateWay();
    return api.doGetWithProd(HomeResource.BasedMarkets);
  }

  async getMarkets(): Promise<any> {
    let api = new APIGateWay();
    return api.doGetWithProd(HomeResource.Markets);
  }

  async getSummaries(): Promise<any> {
    let api = new APIGateWay();
    return api.doGetWithProd(HomeResource.Summaries);
  }
}
