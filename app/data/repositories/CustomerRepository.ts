import { CustomerUseCase, SignInCredential } from 'app/domain/useCases/CustomerUseCase';
import { CustomerResource } from 'app/data/gateway/resource';

import APIGateWay from 'app/data/gateway/APIGateway';

export class CustomerRepository implements CustomerUseCase {
  static create(): CustomerUseCase {
    return new CustomerRepository();
  }

  async signIn(credential: SignInCredential): Promise<any> {
    let api = new APIGateWay();
    return api.doPost(CustomerResource.SignIn, credential);
  }
}
