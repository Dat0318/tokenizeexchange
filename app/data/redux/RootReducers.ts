import { combineReducers } from 'redux';

import { signInReducer } from 'app/data/redux/customer/CustomerReducer';
import {
  getBasedMarketsReducer,
  getSummariesReducer,
  getMarketsReducer
} from 'app/data/redux/home/HomeReducer';

export const rootReducers = {
  customerReducers: combineReducers({
    signInReducer
  }),
  homeReducers: combineReducers({
    getBasedMarketsReducer,
    getSummariesReducer,
    getMarketsReducer,
  }),
};