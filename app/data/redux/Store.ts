import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import { createLogger } from 'redux-logger';
import { createEpicMiddleware } from 'redux-observable';
import { rootReducers } from 'app/data/redux/RootReducers';
import { rootEpics } from 'app/data/redux/RootEpics';

export interface RootState {
}

let store__: any;
export const configureStore = () => {
  const middleware = [];

  const epicMiddleware = createEpicMiddleware();
  middleware.push(epicMiddleware);

  if (__DEV__) {
    middleware.push(createLogger());
  }

  const store = createStore(
    combineReducers(rootReducers), compose(applyMiddleware(...middleware))
  );
  store__ = store;

  epicMiddleware.run(rootEpics);
  return store;
};


export const dispatch = (action: any) => {
  store__.dispatch(action);
};