import { ofType, Epic } from 'redux-observable';
import { map, catchError, exhaustMap } from 'rxjs/operators';
import { from, of } from 'rxjs';

import { HomeRepository } from 'app/data/repositories/HomeRepository';
import { IAction } from 'app/data/redux/common';
import {
  GET_BASED_MARKETS, GET_SUMMARIES, GET_MARKETS,
  getBasedMarketsSuccess, getBasedMarketsFailure,
  getSummariesSuccess, getSummariesFailure,
  getMarketsSuccess, getMarketsFailure
} from 'app/data/redux/home/HomeAction';

export const getBasedMarketsEpic: Epic<any> = (action$: any) => action$.pipe(
  ofType(GET_BASED_MARKETS),
  exhaustMap((action: IAction<any>) => {
    return from(HomeRepository.create().getBaseMarkets()).pipe(
      map(getBasedMarketsSuccess),
      catchError(error => of(getBasedMarketsFailure(error)))
    )
  })
);

export const getSummariesEpic: Epic<any> = (action$: any) => action$.pipe(
  ofType(GET_SUMMARIES),
  exhaustMap((action: IAction<any>) => {
    return from(HomeRepository.create().getSummaries()).pipe(
      map(getSummariesSuccess),
      catchError(error => of(getSummariesFailure(error)))
    )
  })
);

export const getMarketsEpic: Epic<any> = (action$: any) => action$.pipe(
  ofType(GET_MARKETS),
  exhaustMap((action: IAction<any>) => {
    return from(HomeRepository.create().getMarkets()).pipe(
      map(getMarketsSuccess),
      catchError(error => of(getMarketsFailure(error)))
    )
  })
);
