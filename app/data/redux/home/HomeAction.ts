import { IAction } from 'app/data/redux/common';
import { NetError } from 'app/domain/models/NetError';

export const GET_BASED_MARKETS = 'GET_BASED_MARKETS';
export const GET_BASED_MARKETS_SUCCESS = 'GET_BASED_MARKETS_SUCCESS';
export const GET_BASED_MARKETS_FAILURE = 'GET_BASED_MARKETS_FAILURE';

export const getBasedMarkets = (): IAction<any> => ({
  type: GET_BASED_MARKETS,
});

export const getBasedMarketsSuccess = (payload: any): IAction<any> => ({
  type: GET_BASED_MARKETS_SUCCESS,
  payload
});

export const getBasedMarketsFailure = (error: NetError): IAction<NetError> => ({
  type: GET_BASED_MARKETS_FAILURE,
  error
});

export const GET_SUMMARIES = 'GET_SUMMARIES';
export const GET_SUMMARIES_SUCCESS = 'GET_SUMMARIES_SUCCESS';
export const GET_SUMMARIES_FAILURE = 'GET_SUMMARIES_FAILURE';

export const getSummaries = (): IAction<any> => ({
  type: GET_SUMMARIES,
});

export const getSummariesSuccess = (payload: any): IAction<any> => ({
  type: GET_SUMMARIES_SUCCESS,
  payload
});

export const getSummariesFailure = (error: NetError): IAction<NetError> => ({
  type: GET_SUMMARIES_FAILURE,
  error
});

export const GET_MARKETS = 'GET_MARKETS';
export const GET_MARKETS_SUCCESS = 'GET_MARKETS_SUCCESS';
export const GET_MARKETS_FAILURE = 'GET_MARKETS_FAILURE';

export const getMarkets = (): IAction<any> => ({
  type: GET_MARKETS,
});

export const getMarketsSuccess = (payload: any): IAction<any> => ({
  type: GET_MARKETS_SUCCESS,
  payload
});

export const getMarketsFailure = (error: NetError): IAction<NetError> => ({
  type: GET_MARKETS_FAILURE,
  error
});