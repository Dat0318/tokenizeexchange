import { createSelector } from 'reselect';

const selectorRootHomeReducer = createSelector(
  (state: any) => state.homeReducers,
  homeReducers => homeReducers
);

const selectGetBasedMarkets = createSelector(
  state => selectorRootHomeReducer(state),
  homeReducers => homeReducers.getBasedMarketsReducer
);

const selectGetSummaries = createSelector(
  state => selectorRootHomeReducer(state),
  homeReducers => homeReducers.getSummariesReducer
);

const selectGetMarkets = createSelector(
  state => selectorRootHomeReducer(state),
  homeReducers => homeReducers.getMarketsReducer
);

export default {
  selectGetBasedMarkets, selectGetSummaries, selectGetMarkets
};