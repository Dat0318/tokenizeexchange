import {IAction, IState} from 'app/data/redux/common';
import {
  GET_BASED_MARKETS, GET_BASED_MARKETS_SUCCESS, GET_BASED_MARKETS_FAILURE,
  GET_SUMMARIES, GET_SUMMARIES_SUCCESS, GET_SUMMARIES_FAILURE,
  GET_MARKETS, GET_MARKETS_SUCCESS, GET_MARKETS_FAILURE,
} from 'app/data/redux/home/HomeAction';
import ReducerHelper from 'app/data/redux/common/ReducerHelper';

const initialState: IState<any> = ReducerHelper.baseReducer();

export const getBasedMarketsReducer = (state = initialState, action: IAction<any>) => {
  switch (action.type) {
    case GET_BASED_MARKETS:
      return ReducerHelper.baseProcessRequest(state, action);
    case GET_BASED_MARKETS_SUCCESS:
      return ReducerHelper.baseProcessSuccess(state, action);
    case GET_BASED_MARKETS_FAILURE:
      return ReducerHelper.baseProcessFailed(state, action);
    default:
      return state;
  }
}

export const getSummariesReducer = (state = initialState, action: IAction<any>) => {
  switch (action.type) {
    case GET_SUMMARIES:
      return ReducerHelper.baseProcessRequest(state, action);
    case GET_SUMMARIES_SUCCESS:
      return ReducerHelper.baseProcessSuccess(state, action);
    case GET_SUMMARIES_FAILURE:
      return ReducerHelper.baseProcessFailed(state, action);
    default:
      return state;
  }
}

export const getMarketsReducer = (state = initialState, action: IAction<any>) => {
  switch (action.type) {
    case GET_MARKETS:
      return ReducerHelper.baseProcessRequest(state, action);
    case GET_MARKETS_SUCCESS:
      return ReducerHelper.baseProcessSuccess(state, action);
    case GET_MARKETS_FAILURE:
      return ReducerHelper.baseProcessFailed(state, action);
    default:
      return state;
  }
}
