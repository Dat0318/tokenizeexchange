import { IDataList } from 'app/data/redux/common/index';

export default class DataListTransformation {
  static transformListData = (data: Array<any>, idAttribute = '_id'): IDataList<any> => {
    const result: IDataList<any> = {
      byId: {},
      ids: []
    };
    data.forEach(item => {
      const id = item[idAttribute];
      if (result.byId[id] === undefined) {
        result.ids.push(id);
        result.byId[id] = item;
      }
    });

    return result;
  };

  static getDataArray = (data: IDataList<any>): Array<any> => {
    if (data) {
      const ids = data.ids;
      const byId = data.byId;

      return ids.map(id => byId[id]);
    }
    return [];
  };
}
