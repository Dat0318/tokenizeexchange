import { NetError } from 'app/domain/models/NetError';

export const ActionStatus = {
  NONE: 'none',
  FETCHING: 'fetching',
  REFRESHING: 'refreshing',
  DONE: 'done'
};

export interface IAction<T> {
  type: string;
  payload?: T;
  error?: any;
  params?: IActionParams;

  [key: string]: any;
}

export interface IActionParams {
  sectionId?: string;
  isAppend?: boolean;
  canLoadMore?: boolean;

  [key: string]: any;
}

export interface IState<T> {
  isFetching: boolean;
  status: string;
  data?: T;
  params?: any;
  error?: NetError;
  actionType: string;
  success: boolean;
  canLoadMore?: boolean;
}

export interface IDataList<T> {
  byId: {
    [key: string]: T
  };
  ids: Array<string>;

  [key: string]: any;
}

export interface IStateForList<T> extends IState<IDataList<T>> {
  data?: IDataList<T>;
}

export interface IBaseModel {
  forMagentoVersion: string;
}

