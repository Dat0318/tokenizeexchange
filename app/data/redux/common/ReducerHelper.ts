import { ActionStatus, IAction, IDataList, IState, IStateForList } from 'app/data/redux/common';
import { NetError } from 'app/domain/models/NetError';

export default class ReducerHelper {
  static baseReducer = (data?: any): IState<any> => ({
    isFetching: false,
    params: undefined,
    data: data,
    error: undefined,
    success: false,
    actionType: '',
    status: ActionStatus.NONE
  });

  static baseProcessRequest = (state: IState<any>, action: IAction<any>) => {
    return {
      ...state,
      status: action.params && action.params.refresh ? ActionStatus.REFRESHING : ActionStatus.FETCHING,
      isFetching: true,
      params: action.payload,
      error: undefined,
      success: false,
      actionType: action.type
    };
  };

  static baseProcessSuccess = (state: IState<any>, action: IAction<any>) => {
    return {
      ...state,
      status: ActionStatus.DONE,
      isFetching: false,
      data: action.payload,
      error: undefined,
      success: true,
      actionType: action.type
    };
  };

  static baseProcessFailed = (state: IState<any>, action: IAction<NetError>) => {
    return {
      ...state,
      status: ActionStatus.DONE,
      isFetching: false,
      error: action.error,
      success: false,
      actionType: action.type
    };
  };

  static baseListReducer = (data?: IDataList<any>): IState<IDataList<any>> => ({
    isFetching: false,
    params: undefined,
    data: data ? data : {
      byId: {},
      ids: []
    },
    error: undefined,
    success: false,
    actionType: '',
    status: ActionStatus.NONE
  });

  static baseProcessForListSuccess = (state: IStateForList<any>, action: IAction<IDataList<any>>, isAppend?: boolean) => {
    let newData = Object.assign({}, state.data);
    const listData: IDataList<any> = newData ? newData : {byId: {}, ids: []};
    const byId = action.payload ? action.payload.byId : {};
    const ids = action.payload ? action.payload.ids : [];

    if (isAppend) {
      listData.ids = listData.ids.concat(ids);
      listData.byId = {
        ...listData.byId,
        ...byId
      };
    } else {
      listData.ids = ids;
      listData.byId = byId;
    }

    newData = listData;
    const result: IStateForList<any> = {
      ...state,
      status: ActionStatus.DONE,
      isFetching: false,
      data: newData,
      error: undefined,
      success: true,
      actionType: action.type,
      canLoadMore: action.params ? !!action.params.canLoadMore : true
    };

    return result;
  };
}
