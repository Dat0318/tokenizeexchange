import { SignInCredential } from 'app/domain/useCases/CustomerUseCase';
import { IAction } from 'app/data/redux/common';
import { NetError } from 'app/domain/models/NetError';

export const SIGN_IN = 'SIGN_IN';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const signIn = (payload: SignInCredential): IAction<SignInCredential> => ({
  type: SIGN_IN,
  payload
});

export const signInSuccess = (payload: any): IAction<any> => ({
  type: SIGN_IN_SUCCESS,
  payload
});

export const signInFailure = (error: NetError): IAction<NetError> => ({
  type: SIGN_IN_FAILURE,
  error
});