import { ofType, Epic } from 'redux-observable';
import { map, catchError, exhaustMap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { CustomerRepository } from 'app/data/repositories/CustomerRepository';
import { SIGN_IN, signInSuccess, signInFailure } from 'app/data/redux/customer/CustomerAction';
import { IAction } from 'app/data/redux/common';

export const signInEpic: Epic<any> = (action$: any) => action$.pipe(
  ofType(SIGN_IN),
  exhaustMap((action: IAction<any>) => {
    return from(CustomerRepository.create().signIn(action.payload)).pipe(
      map(signInSuccess),
      catchError(error => of(signInFailure(error)))
    )
  })
);
