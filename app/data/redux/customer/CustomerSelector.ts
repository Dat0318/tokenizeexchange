import { createSelector } from 'reselect';

const selectorRootCustomerReducer = createSelector(
  (state: any) => state.customerReducers,
  customerReducers => customerReducers
);

const selectSignIn = createSelector(
  state => selectorRootCustomerReducer(state),
  customerReducers => customerReducers.signInReducer
);

export default {
  selectSignIn
};