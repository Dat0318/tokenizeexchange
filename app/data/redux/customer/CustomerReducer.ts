import {IAction, IState} from 'app/data/redux/common';
import {
  SIGN_IN, SIGN_IN_SUCCESS, SIGN_IN_FAILURE
} from 'app/data/redux/customer/CustomerAction';
import ReducerHelper from 'app/data/redux/common/ReducerHelper';

const initialState: IState<any> = ReducerHelper.baseReducer();

export const signInReducer = (state = initialState, action: IAction<any>) => {
  switch (action.type) {
    case SIGN_IN:
      return ReducerHelper.baseProcessRequest(state, action);
    case SIGN_IN_SUCCESS:
      return ReducerHelper.baseProcessSuccess(state, action);
    case SIGN_IN_FAILURE:
      return ReducerHelper.baseProcessFailed(state, action);
    default:
      return state;
  }
}
