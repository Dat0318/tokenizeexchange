import { combineEpics } from 'redux-observable';

import { signInEpic } from 'app/data/redux/customer/CustomerEpic';
import { getBasedMarketsEpic, getMarketsEpic, getSummariesEpic } from 'app/data/redux/home/HomeEpic';

export const rootEpics = combineEpics(
  combineEpics(
    signInEpic
  ),
  combineEpics(
    getBasedMarketsEpic,
    getSummariesEpic,
    getMarketsEpic,
  ),
)