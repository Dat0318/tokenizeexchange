import axios, { AxiosResponse, AxiosError } from 'axios';

import { AppConfig } from 'app/data/config/Config';
import { NetError } from 'app/domain/models/NetError';
import { isMockEnabled, mockData } from 'app/data/mock/APIMock';

import ResponseModel from 'app/common/response/ResponseModel';

export default class APIGateWay {
  private _timeOut = 30 * 1000;
  _apiConfig: AppConfig;

  constructor() {
    // @ts-ignore
    this._apiConfig = AppConfig.getAppConfig();
  }

  _handleSuccess = (response: AxiosResponse) => {
    console.log('GET_handleSuccess', response);
    const { status, data } = response;
    if (status >= 200 && status < 300) {
      return data;
    } else {
      let error: NetError = { status: status, message: '' };
      return Promise.reject(error);
    }
  };

  _handleError = (axiosError: AxiosError) => {
    const { response, request } = axiosError;
    let status;
    let errorMessage;
    if (response) {
      status = response.data?.status || 400;
      errorMessage = response.data?.error || '';
    } else if (request) {
      status = 500;
      errorMessage = 'There is an application error';
    } else {
      status = 500;
      errorMessage = 'There is an application error';
    }
    return Promise.reject(ResponseModel.createError(status, errorMessage));
  };

  doGet = (resource: string) => {
    const { endpoint, ignoreMock } = this._apiConfig;
    const axiosConfig = {
      baseURL: endpoint,
      timeout: this._timeOut,
      headers: { 'Content-Type': 'application/json' },
    };
    console.log('doGet_loggger: ', resource);
    if (ignoreMock && isMockEnabled(resource)) {
      return mockData(resource);
    }
    return axios
      .get(resource, axiosConfig)
      .then(this._handleSuccess)
      .catch(this._handleError);
  };

  doGetWithProd = (resource: string) => {
    const { endpointProd, ignoreMock } = this._apiConfig;
    const axiosConfig = {
      baseURL: endpointProd,
      timeout: this._timeOut,
      headers: { 'Content-Type': 'application/json' },
    };
    console.log('doGetWithProd_loggger: ', resource);
    if (ignoreMock && isMockEnabled(resource)) {
      return mockData(resource);
    }
    return axios
      .get(resource, axiosConfig)
      .then(this._handleSuccess)
      .catch(this._handleError);
  };

  doPost = (resource: string, requestBody: any) => {
    const { endpoint, ignoreMock } = this._apiConfig;
    const axiosConfig = {
      baseURL: endpoint,
      timeout: this._timeOut,
      headers: { 'Content-Type': 'application/json' },
    };
    console.log('doPost_loggger: ', resource);
    if (ignoreMock && isMockEnabled(resource)) {
      return mockData(resource);
    }
    return axios
      .post(resource, requestBody, axiosConfig)
      .then(this._handleSuccess)
      .catch(this._handleError);
  };

}