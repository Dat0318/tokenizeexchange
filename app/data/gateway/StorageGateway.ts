import AsyncStorage from '@react-native-async-storage/async-storage';

export default class StorageGateway {
  get = (key: string): Promise<string | null> => {
    return AsyncStorage.getItem(key);
  };

  saveString = (key: string, value: string) => {
    return AsyncStorage.setItem(key, value);
  };

  saveObject = (key: string, objData: object) => {
    return AsyncStorage.setItem(key, JSON.stringify(objData));
  };

  clear = (key: string) => {
    return AsyncStorage.removeItem(key);
  };
}
