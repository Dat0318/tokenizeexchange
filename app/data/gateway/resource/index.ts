export const HomeResource = {
  BasedMarkets: 'mobile-api/market/getbasedmarkets',
  Summaries: 'public/v1/market/get-summaries',
  Markets: 'mobile-api/market/getmarkets',
};

export const CustomerResource = {
  SignIn: 'mobile-api/auth/login',
};