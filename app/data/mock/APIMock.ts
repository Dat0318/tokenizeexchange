import { CustomerResource, HomeResource } from 'app/data/gateway/resource';

const mocks = [
    {
        path: CustomerResource.SignIn,
        data: require('./data/signIn.json'),
        enable: false,
    },
    {
        path: HomeResource.BasedMarkets,
        data: require('./data/signIn.json'),
        enable: false,
    },
    {
        path: HomeResource.Markets,
        data: require('./data/signIn.json'),
        enable: false,
    },
    {
        path: HomeResource.Summaries,
        data: require('./data/signIn.json'),
        enable: false,
    }
];

export const isMockEnabled = (path: string) => {
    let filteredMocks = mocks.filter(x => x.path == path);
    return filteredMocks.length && filteredMocks[0].enable;
}

export const mockData = (path: string) => {
    let filteredMocks = mocks.filter(x => x.path == path);
    if (!filteredMocks.length) {
        return Promise.reject({status: 404, message: 'Mock data not found'});
    }
    let mock = filteredMocks[0];
    if (!mock.data) {
        return Promise.reject({status: 404, message: 'Mock data not found'});
    }
    return Promise.resolve(mock.data);
}
