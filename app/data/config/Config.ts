const Env = {
  Integration: 1,
  UAT: 2,
  Production: 3,
};

const CURRENT_ENV = Env.Integration;

export interface AppConfig {
  endpoint: string;
  endpointProd: string;
  ignoreMock: boolean;
}

const StagingConfig = {
  endpoint: 'https://api.tokenize-dev.com/',
  endpointProd: 'https://api2.tokenize.exchange/',
  ignoreMock: true,
};
const UATConfig = {
  endpoint: 'https://api.tokenize-dev.com/',
  endpointProd: 'https://api2.tokenize.exchange/',
  ignoreMock: false,
};
const ProdConfig = {
  endpoint: 'https://api2.tokenize.exchange/',
  endpointProd: 'https://api2.tokenize.exchange/',
  ignoreMock: false,
};

export const getAppConfig = () => {
  switch (CURRENT_ENV) {
    case Env.Integration:
      return StagingConfig;
    case Env.UAT:
      return UATConfig;
    case Env.Production:
      return ProdConfig;
  }
};


export const AppConfig = {
  getAppConfig,
};
