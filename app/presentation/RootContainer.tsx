import React, { useEffect, useState } from 'react';

import styled from 'styled-components/native';
import { NavigationContainer } from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';
import DropdownAlert from 'react-native-dropdownalert';

import { NavigationController } from 'app/presentation/navigation/NavigationController';
import { AppNavManager } from 'app/presentation/navigation/AppNavManager';
import { appColors } from 'app/presentation/theme/Theme';
import { NetworkOfflineView } from 'app/presentation/component/network/NetworkOfflineView';

import DropDownHolder from 'app/common/helper/DropDownHolder';

const RootContainer = () => {
  const [isNetworkOffline, setNetworkConnect] = useState(false);

  useEffect(() => {
    NetInfo.addEventListener(state => {
      setNetworkConnect(!state.isConnected);
    });
  }, []);

  return (
    <NavigationContainer ref={NavigationController.navigationRef}>
      {AppNavManager.createAppNavigator()}
      {isNetworkOffline && <NetworkOfflineView/>}
      <DropdownAlert messageNumOfLines={5}
                     useNativeDriver={true}
                     updateStatusBar={false}
                     ref={(ref: DropdownAlert) => DropDownHolder.setDropDown(ref)}/>
    </NavigationContainer>
  );
};
export default RootContainer;

const ViewContainer = styled.View`
  flex: 1;
  background-color: ${appColors.Backdrop};
`;