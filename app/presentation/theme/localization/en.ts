export default {
  // region Message
  signInSuccess: 'Signin Success',
  success: 'Success',
  error: 'Error',
  info: 'Information',
  internetConnectionLost: 'Internet Connection Lost',
  missingInput: 'Missing Input',
  // endregion

  // region SignIn
  email: 'Email',
  password: 'Password',
  signIn: 'Sign\nIn',
  signInNormal: 'Sign in',
  plzSignInContinue: 'Please sign in to continue',
  rememberMe: 'Remember me',
  forgotPassword: 'Forgot your password?',
  signUp: 'Dont have an account yet? SIGN\nUP',
  // endregion

  // region Home
  home: 'Home',
  // endregion

  // region Markets
  markets: 'Markets',
  // endregion

  // region Wallets
  wallets: 'Wallets',
  // endregion

  // region Portfolio
  portfolio: 'Portfolio',
  // endregion

  // region More
  more: 'More',
  // endregion
};
