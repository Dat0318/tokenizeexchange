import I18n from 'react-native-i18n';

import vi from 'app/presentation/theme/localization/vi';
import en from 'app/presentation/theme/localization/en';

const configureLocalization = () => {
    I18n.fallbacks = true;
    I18n.defaultSeparator = '.';
    I18n.translations = {
        en,
        vi,
    };
};

const getString = (key: string, params: any) => {
    // @ts-ignore
    const realKey = Object.keys(en).find(k => en[k] === key) || '';
    return I18n.t(realKey, params);
};

const appString = en;

const languages = {
    ENGLISH: 'en',
    VIETNAM: 'vi',
};

export const Localizations = {
    configureLocalization,
    languages,
    getString,
    appString,
};



