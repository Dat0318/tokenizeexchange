const colors = {
  White: '#FFFFFF',
  Black: '#000000',
  Transparent: '#00000000',
  Backdrop: 'rgba(0, 0, 0, 0.3)',
  SolidButton: '#BDCFFF',
  TextInput: 'rgba(97, 157, 255, 0.5)',
  Hex: {
    // Tabbar
    Hex6081FA: '#6081FA',
    Hex9194BB: '#9194BB',

    Hex777777: '#777777',
    HexF3F3F3: '#F3F3F3',
  },
  Gray: {
    C50: '#F0F4F6',
    C100: '#F9F9F9',
    C150: '#F1F4F6',
    C160: '#d4d4d4',
    C180: '#D8D8D8',
    C170: '#656667',
    C190: '#9B9B9B',
    C200: '#8E8E8E',
    C300: '#9c9c9c',
    C500: '#5E5E5E',
    C700: '#2B343A',
    C800: '#808080',
    C900: '#080808',
  },
  PrimaryTextColor: '#231F20',
};
export default colors;