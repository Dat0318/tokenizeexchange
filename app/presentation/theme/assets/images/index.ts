import Home from 'app/presentation/theme/assets/images/ic_home.svg';
import ActiveHome from 'app/presentation/theme/assets/images/ic_active_home.svg';
import Markets from 'app/presentation/theme/assets/images/ic_markets.svg';
import ActiveMarkets from 'app/presentation/theme/assets/images/ic_active_markets.svg';
import Wallets from 'app/presentation/theme/assets/images/ic_wallets.svg';
import ActiveWallets from 'app/presentation/theme/assets/images/ic_active_wallets.svg';
import Portfolio from 'app/presentation/theme/assets/images/ic_portfolio.svg';
import ActivePortfolio from 'app/presentation/theme/assets/images/ic_active_portfolio.svg';
import More from 'app/presentation/theme/assets/images/ic_more.svg';
import ActiveMore from 'app/presentation/theme/assets/images/ic_active_more.svg';
import Email from 'app/presentation/theme/assets/images/ic_email.svg';
import Password from 'app/presentation/theme/assets/images/ic_password.svg';
import Bkg from 'app/presentation/theme/assets/images/bkg.svg';
import Logo from 'app/presentation/theme/assets/images/ic_logo.svg';
import Search from 'app/presentation/theme/assets/images/ic_search.svg';
import Checkbox from 'app/presentation/theme/assets/images/ic_checkbox.svg';
import ActiveCheckbox from 'app/presentation/theme/assets/images/ic_check_white.svg';

// Icon Coin
import Bitcoin from 'app/presentation/theme/assets/images/coin/ic_bitcoin.svg';
import Ethereum from 'app/presentation/theme/assets/images/coin/ic_ethereum.svg';
import Litecoin from 'app/presentation/theme/assets/images/coin/ic_litecoin.svg';
import OmiceGo from 'app/presentation/theme/assets/images/coin/ic_omiceGo.svg';
import Ripple from 'app/presentation/theme/assets/images/coin/ic_ripple.svg';
import Zcash from 'app/presentation/theme/assets/images/coin/ic_zcash.svg';
import Zilliqa from 'app/presentation/theme/assets/images/coin/ic_zilliqa.svg';

export default {
  Logo: {
    LogoSVG: Logo,
  },
  Icons: {
    HomeSVG: Home,
    ActiveHomeSVG: ActiveHome,
    MarketsSVG: Markets,
    ActiveMarketsSVG: ActiveMarkets,
    WalletsSVG: Wallets,
    ActiveWalletsSVG: ActiveWallets,
    PortfolioSVG: Portfolio,
    ActivePortfolioSVG: ActivePortfolio,
    MoreSVG: More,
    ActiveMoreSVG: ActiveMore,
    EmailSVG: Email,
    PasswordSVG: Password,
    SearchSVG: Search,
    CheckboxSVG: Checkbox,
    CheckboxWhiteSVG: ActiveCheckbox,
    // Coin Icon
    BitcoinSVG: Bitcoin,
    EthereumSVG: Ethereum,
    LitecoinSVG: Litecoin,
    OmiceGoSVG: OmiceGo,
    RippleSVG: Ripple,
    ZcashSVG: Zcash,
    ZilliqaSVG: Zilliqa,
  },
  Bkg: {
    BkgSVG: Bkg,
    BkgPNG: require('app/presentation/theme/assets/images/background.png'),
  }
}