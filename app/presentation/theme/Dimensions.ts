import { PixelRatio } from 'react-native';

const spacingSize = {
  Tiny: 4,
  TinyX: 6,
  Small: 8,
  SmallX: 10,
  Medium: 12,
  MediumX: 14,
  Large: 16,
  LargeX: 18,
  LargeXX: 24,
  Huge: 28,
  HugeX: 32,
  HugeXX: 34,
};

const fontSize = {
  Mini: 8,
  MiniX: 9,
  Tiny: 10,
  TinyX: 11,
  Small: 12,
  SmallX: 13,
  Medium: 14,
  MediumX: 15,
  Large: 16,
  LargeX: 18,
  Huge: 24,
  HugeX: 28,
  HugeXX: 32,
};

const size = {
  Min: 1 / PixelRatio.get(),
};


export default {
  Spacing: spacingSize,
  Font: fontSize,
  Size: size,
};
