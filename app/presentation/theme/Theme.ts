import themeColor from 'app/presentation/theme/Colors';
import Image from 'app/presentation/theme/assets/images';
import themeDimension from 'app/presentation/theme/Dimensions';
import { Localizations } from 'app/presentation/theme/Localizations';

let themeDefault = {
  common: {},
  navigation: {},
  font: {},
};

export const createAppTheme = () => {
  return themeDefault;
};

export const appColors = themeColor;
export const appDimensions = themeDimension;

export const configureLocalization = () => Localizations.configureLocalization();
export const getString = (key: string, params?: any) => Localizations.getString(key, params);

export const appString = Localizations.appString;
export const appLanguages = Localizations.languages;

export const appImage = Image;


