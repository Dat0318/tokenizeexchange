import React from 'react';

import styled from 'styled-components/native';

import { appImage } from 'app/presentation/theme/Theme';

export const Logo = (props: any) => {
  const IconLogo = appImage.Logo.LogoSVG;
  const {height, marginTop, marginBottom} = props;
  return (
    <ViewLogoContainer height={height} marginTop={marginTop}
                       marginBottom={marginBottom}>
      <IconLogo height={64} width={64}/>
    </ViewLogoContainer>
  );
}

const ViewLogoContainer = styled.View.attrs((props: any) => ({
  height: props.height| 64 ,
  marginTop: props.marginTop || 0,
  marginBottom: props.marginBottom || 0,
}))`
  height: ${props => props.height};
  margin-top: ${props => props.marginTop};
  margin-bottom: ${props => props.marginBottom};
  align-items: center;
  justify-content: center;
`;