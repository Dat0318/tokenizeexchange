import React from 'react';
import styled from 'styled-components/native';
import { appColors } from 'app/presentation/theme/Theme';

export const TextPrimary = styled.Text.attrs((props: any) => ({
  color: props.color || appColors.Black,
  fontFamily: props.fontFamily || 'Roboto-Medium',
  textTransform: props.textTransform || 'none',
  letterSpacing: props.letterSpacing || 0,
}))`
  color: ${props => props.color};
  font-family: ${props => props.fontFamily};
  text-transform: ${props => props.textTransform};
  letter-spacing: ${props => props.letterSpacing};
`;