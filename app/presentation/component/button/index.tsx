import React from 'react';

import { Button } from 'react-native-elements';
import styled from 'styled-components/native';

import { appColors, appDimensions } from 'app/presentation/theme/Theme';
import { TextPrimary } from 'app/presentation/component/text';
import { getWindowWidth } from 'app/common/helper/WindowSizeHelper';

export const SolidButton = (props: any) => {
  const { containerStyle, titleStyle } = props;
  return (
    <Button
      {...props}
      buttonStyle={{
        backgroundColor: appColors.SolidButton,
      }}
      containerStyle={{
        borderRadius: appDimensions.Spacing.Small,
        ...containerStyle,
      }}
      titleStyle={titleStyle ? titleStyle : {
        fontWeight: 'normal',
        fontSize: appDimensions.Font.Large,
      }}
    />
  );
};

export const TabButton = (props: any) => {
  const { onPress, title, active } = props;
  return (
    <TabButtonContainer onPress={onPress} active={active}>
      <TabButtonTitle active={active}>{title}</TabButtonTitle>
    </TabButtonContainer>
  );
};

const TabButtonContainer = styled.TouchableOpacity`
  width: ${getWindowWidth() / 6};
  margin-vertical: ${appDimensions.Spacing.Medium};
  padding-top: ${appDimensions.Spacing.Small};
  padding-bottom: ${appDimensions.Spacing.Small};
  border-radius: ${appDimensions.Spacing.Small};
  border-width: ${1};
  backgroundColor: ${props => props.active ? appColors.Gray.C500 : appColors.Transparent};
  margin-left: ${appDimensions.Spacing.Large};
  margin-right: ${appDimensions.Spacing.Large};
`;

const TabButtonTitle = styled(TextPrimary)`
  color: ${props => props.active ? appColors.White : appColors.Black};
  font-size: ${appDimensions.Font.Medium};
  font-weight: bold;
  text-align: center;
`;
