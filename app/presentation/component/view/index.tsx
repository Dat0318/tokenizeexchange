import styled from 'styled-components/native';
import { appColors, appDimensions } from 'app/presentation/theme/Theme';

const getBackgroundColor = (props: any) => {
  const {type, color} = props;
  if (undefined === type || null === type || 'default' === type) {
    return appColors.White;
  } else {
    return color;
  }
};

const getFlex = (props: any) => {
  const {center, flex} = props;
  if (center) {
    return 'center';
  } else {
    return flex;
  }
};

export const ViewContainer = styled.View`
  flex: 1;
  align-items: ${(props: any) => getFlex(props)};
  justify-content: ${(props: any) => getFlex(props)};
  background-color: ${(props: any) => getBackgroundColor(props)};
`;

export const ViewContainer1 = styled.View`
  flex: 1;
  background-color: ${(props: any) => getBackgroundColor(props)};
`;

export const ViewSeparator = styled.View`
  height: ${appDimensions.Size};
  backgroundColor: ${(props: any) => props.color ? props.color : appColors.White};
`;
