import React, { useEffect } from 'react';

import { CheckBox } from 'react-native-elements';
import styled from 'styled-components/native';
import { appColors, appDimensions, appImage } from 'app/presentation/theme/Theme';

const CheckboxNormal = (props: any) => {
  const { title, iconRight, isSelected, onChecked } = props;
  const IconCheckWhite = appImage.Icons.CheckboxWhiteSVG;

  const checkedIcon = () => {
    return (
      <ViewBoxCheck>
        <IconCheckWhite width={24} height={24}/>
      </ViewBoxCheck>
    );
  };

  const uncheckedIcon = () => {
    return (
      <ViewBoxCheck/>
    );
  };

  const onPressCheck = () => {
    if (onChecked) {
      onChecked(true);
    }
  };

  return (
    <CheckBox
      iconRight={iconRight ? false : true}
      title={title}
      checkedIcon={checkedIcon()}
      uncheckedIcon={uncheckedIcon()}
      textStyle={{
        color: appColors.White,
        fontWeight: '400',
        fontSize: appDimensions.Font.Large,
      }}
      containerStyle={{
        backgroundColor: 'transparent',
        borderWidth: 0,
        paddingLeft: 7,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
      }}
      onPress={onPressCheck}
      checked={isSelected}/>
  );
};
export default React.memo(CheckboxNormal);

const ViewBoxCheck = styled.View`
  height: ${24};
  width: ${24};
  background-color: ${appColors.TextInput};
  justify-content: center;
  align-items: center;
  border-radius: ${4};
`;