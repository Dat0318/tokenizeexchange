import React from 'react';
import { Platform } from 'react-native';

import styled from 'styled-components/native';

import { appColors, appDimensions, appString, getString } from 'app/presentation/theme/Theme';

export const NetworkOfflineView = () => {
  return (
    <ContainerView>
      <NetworkText>{getString(appString.internetConnectionLost)}</NetworkText>
    </ContainerView>
  );
};

const ContainerView = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  align-items: center;
  justify-content: center;
  top: ${Platform.select({ios: 88, android: 48})};
  height: ${appDimensions.Spacing.LargeXX};
  background-color: ${appColors.Hex.Hex777777};
`;

const NetworkText = styled.Text`
  color: ${appColors.White}
  fontSize: ${appDimensions.Font.Small};
`;