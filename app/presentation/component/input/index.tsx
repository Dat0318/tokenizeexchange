import React, { useState, useRef } from 'react';

import { Input, Icon } from 'react-native-elements';
import { appColors, appDimensions, appImage } from 'app/presentation/theme/Theme';

const ImageEmail = appImage.Icons.EmailSVG;
const ImagePassword = appImage.Icons.PasswordSVG;

export const TextInput = (props: any) => {
  const [labelVisible, setLabelVisible] = useState(false);
  const {onTextChanged, labelEnabled, forwardRef, inputStyle, labelRight, containerStyle} = props;
  const textColor = appColors.White;
  const labelColor = labelVisible && labelEnabled ? textColor : appColors.Transparent;
  const inputStyles = inputStyle ?
    {...inputStyle, color: textColor, padding: 0, margin: 0}
    :
    {color: textColor, padding: 0, margin: 0, fontSize: appDimensions.Font.MediumX};
  let inputRef = null === forwardRef ? useRef() : forwardRef;

  const renderIcon = () => {
    return <ImageEmail style={{
      marginRight: appDimensions.Spacing.Medium
    }}/>;
  };

  return (
    <Input
      {...props}
      ref={inputRef}
      inputStyle={inputStyles}
      inputContainerStyle={{
        borderBottomColor: appColors.Transparent,
        borderBottomWidth: appDimensions.Size.Min
      }}
      labelStyle={{
        color: labelColor, fontWeight: 'normal',
        fontFamily: 'Roboto-Medium'
      }}
      placeholderTextColor={textColor}
      containerStyle={{
        paddingHorizontal: appDimensions.Spacing.Large,
        height: 50, borderRadius: appDimensions.Spacing.Small,
        ...containerStyle
      }}
      maxLength={50}
      leftIcon={renderIcon}
      onChangeText={(text: string) => {
        if (labelEnabled) {
          if (text && text.length > 0) {
            setLabelVisible(true);
          } else {
            setLabelVisible(false);
          }
        }
        onTextChanged && onTextChanged(text);
      }}
    />
  );
};

export const PasswordInput = (props: any) => {
  const [labelVisible, setLabelVisible] = useState(false);
  const [isPassword, setIsPassword] = useState(true);
  const {onTextChanged, labelEnabled, forwardRef, inputStyle, containerStyle} = props;
  const textColor = appColors.White;
  const labelColor = labelVisible && labelEnabled ? textColor : appColors.Transparent;
  const rightIconName = isPassword ? 'eye-slash' : 'eye';
  const inputStyles = inputStyle ?
    {...inputStyle, color: textColor}
    :
    {color: textColor, fontSize: appDimensions.Font.MediumX};
  let inputRef = null === forwardRef ? useRef() : forwardRef;

  const renderIcon = () => {
    return <ImagePassword style={{
      marginRight: appDimensions.Spacing.Medium
    }}/>;
  };

  return (
    <Input
      {...props}
      ref={inputRef}
      inputStyle={inputStyles}
      inputContainerStyle={{
        borderBottomColor: appColors.Transparent,
        borderBottomWidth: appDimensions.Size.Min,
      }}
      labelStyle={{
        color: labelColor, fontWeight: 'normal',
        fontFamily: 'Roboto-Medium'
      }}
      containerStyle={{
        paddingHorizontal: appDimensions.Spacing.Large,
        height: 50, borderRadius: appDimensions.Spacing.Small,
        ...containerStyle,
      }}
      placeholderTextColor={textColor}
      secureTextEntry={isPassword}
      maxLength={30}
      leftIcon={renderIcon}
      rightIcon={
        <Icon type="font-awesome"
              size={appDimensions.Spacing.LargeX}
              name={rightIconName}
              color={appColors.White}
              onPress={() => {
                setIsPassword(!isPassword);
              }}
        />
      }
      onChangeText={(text: string) => {
        if (labelEnabled) {
          if (text && text.length > 0) {
            setLabelVisible(true);
          } else {
            setLabelVisible(false);
          }
        }
        onTextChanged && onTextChanged(text);
      }}
    />
  );
};