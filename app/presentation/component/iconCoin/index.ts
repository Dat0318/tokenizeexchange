import { appImage } from 'app/presentation/theme/Theme';
import { BasedCoin } from 'app/common/constant/Constants';

let Icon = null;

const ImageHome = appImage.Icons.HomeSVG;
const ImageBitcoin = appImage.Icons.BitcoinSVG;
const ImageEthereum = appImage.Icons.EthereumSVG;
const ImageLitecoin = appImage.Icons.LitecoinSVG;
const ImageOmiceGo = appImage.Icons.OmiceGoSVG;
const ImageRipple = appImage.Icons.RippleSVG;
const ImageZcash = appImage.Icons.ZcashSVG;
const ImageZilliqa = appImage.Icons.ZilliqaSVG;

const getIconCoin = (coin: string) => {
  switch (coin) {
    case BasedCoin.Bitcoin:
      Icon = ImageBitcoin;
      break;
    case BasedCoin.Ethereum:
      Icon = ImageEthereum;
      break;
    case BasedCoin.Litecoin:
      Icon = ImageLitecoin;
      break;
    case BasedCoin.OmiceGo:
      Icon = ImageOmiceGo;
      break;
    case BasedCoin.Ripple:
      Icon = ImageRipple;
      break;
    case BasedCoin.Zcash:
      Icon = ImageZcash;
      break;
    case BasedCoin.Zilliqa:
      Icon = ImageZilliqa;
      break;
    default:
      Icon = ImageHome;
      break;
  }
  return Icon;
};

export default getIconCoin;