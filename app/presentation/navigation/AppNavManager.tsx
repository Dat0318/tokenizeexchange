import React from 'react';

import { Routes } from 'app/presentation/navigation/Routes';
import { MainNavigator } from 'app/presentation/navigation/navigator/MainNavigator';

import { createStackNavigator } from '@react-navigation/stack';
import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { createAuthStack } from 'app/presentation/navigation/stack/AuthStack';
import { createBottomTabs } from 'app/presentation/navigation/tab';

const createAppNavigator = () => {
  const StackNavigator = createStackNavigator();
  return (
    <StackNavigator.Navigator initialRouteName={Routes.Root.Start} headerMode={'none'}>
      <StackNavigator.Screen name={Routes.Root.Start} component={AppScreens.Main.Start}/>
      <StackNavigator.Screen name={Routes.Root.AuthStack} component={createAuthStack}/>
      <StackNavigator.Screen name={Routes.Root.Main} component={createBottomTabs} />
    </StackNavigator.Navigator>
  );
};

export const AppNavManager = {
  createAppNavigator,
  MainNavigator,
};
