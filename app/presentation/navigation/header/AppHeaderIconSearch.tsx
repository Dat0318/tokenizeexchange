import React from 'react';

import { appDimensions, appImage } from 'app/presentation/theme/Theme';

const AppHeaderIconSearch = (props: any) => {
  const ImgSearch = appImage.Icons.SearchSVG;
  return <ImgSearch style={{
    marginRight: appDimensions.Spacing.LargeXX
  }}/>;
};

export default React.memo(AppHeaderIconSearch);
