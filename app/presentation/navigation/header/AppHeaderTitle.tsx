import React from 'react';

import styled from 'styled-components/native';

import { appDimensions } from 'app/presentation/theme/Theme';
import { TextPrimary } from 'app/presentation/component/text';

const AppHeaderTitle = (props: any) => {
  const {title} = props;
  return (
      <TextTitle>{title}</TextTitle>
  );
};

export default React.memo(AppHeaderTitle);

const TextTitle = styled(TextPrimary)`
  padding-left: ${appDimensions.Spacing.HugeXX};
  font-size: ${appDimensions.Font.LargeX};
  font-weight: ${'400'};
  font-family: 'Roboto-Bold';
`;
