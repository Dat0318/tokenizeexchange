import React from 'react';

import { Platform } from 'react-native';

import { appColors, appDimensions } from 'app/presentation/theme/Theme';
import AppHeaderTitle from 'app/presentation/navigation/header/AppHeaderTitle';
import AppHeaderIconSearch from 'app/presentation/navigation/header/AppHeaderIconSearch';

export const defaultHeaderOptions = {
  headerTintColor: appColors.White,
  headerBackTitleVisible: false,
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

export const defaultHeaderStyle = {
  shadowColor: appColors.Transparent,
  shadowRadius: 0,
  shadowOffset: {
    height: 0,
  },
};

export const headerStyle = {
  backgroundColor: appColors.White,
  height: Platform.OS === 'ios' ? 90 : 48,
  shadowColor: appColors.Black,
  shadowOpacity: 0.2,
  shadowOffset: {
    height: 2,
  },
  shadowRadius: 2,
  elevation: 5
};

export const headerStyleNoShaw = {
  backgroundColor: appColors.White,
  height: Platform.OS === 'ios' ? 90 : 48,
  shadowOffset: {
    height: 0,
  },
  shadowRadius: 0,
  elevation: 0
};

export const headerTitleStyle = {
  fontSize: appDimensions.Spacing.Large,
  fontFamily: 'Montserrat-SemiBold',
  textAlign: 'center'
};

const makeMainScreenHeader = (textTitle: string) => {
  return {
    headerLeft: () => <AppHeaderTitle title={textTitle}/>,
    headerRight: () => <AppHeaderIconSearch />,
    headerTitle: null,
    headerTitleStyle: null,
    headerShown: true,
    headerBackTitleVisible: false,
    gestureEnabled: true,
    headerStyle: { ...headerStyle },
  };
};

export const HeaderHelper = {
  makeMainScreenHeader
}