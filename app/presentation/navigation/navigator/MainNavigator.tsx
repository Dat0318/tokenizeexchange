import { NavigationController } from 'app/presentation/navigation/NavigationController';
import { Routes } from 'app/presentation/navigation/Routes';

const openAuthStackWithWalkThrough = () => {
  NavigationController.navigateWithReset(Routes.Root.AuthStack);
};

const openMain = () => {
  NavigationController.navigateWithReset(Routes.Root.Main);
};

const openHomeStack = () => {
  NavigationController.navigate(Routes.Main.Home);
};

const openMarketsStack = () => {
  NavigationController.navigate(Routes.Main.Markets);
};

const openWalletsStack = () => {
  NavigationController.navigate(Routes.Main.Wallets);
};

const openPortfolioStack = () => {
  NavigationController.navigate(Routes.Main.Portfolio);
};

const openMoreStack = () => {
  NavigationController.navigate(Routes.Main.More);
};

export const MainNavigator = {
  openAuthStackWithWalkThrough, openMain, openHomeStack, openMarketsStack,
  openWalletsStack, openPortfolioStack, openMoreStack
};