import { NavigationController } from 'app/presentation/navigation/NavigationController';
import { Routes } from 'app/presentation/navigation/Routes';

const openAuth = () => {
  NavigationController.navigate(Routes.Auth.SignIn);
};

export const AuthNavigator = {
  openAuth
};