import { createHomeStack } from 'app/presentation/navigation/stack/HomeStack';
import { createMarketStack } from 'app/presentation/navigation/stack/MarketsStack';
import { createWalletsStack } from 'app/presentation/navigation/stack/WalletsStack';
import { createPortfolioStack } from 'app/presentation/navigation/stack/PortfolioStack';
import { createMoreStack } from 'app/presentation/navigation/stack/MoreStack';

export const AppStack = {
  createHomeStack,
  createMarketStack,
  createWalletsStack,
  createPortfolioStack,
  createMoreStack,
};
