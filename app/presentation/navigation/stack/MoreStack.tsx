import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { HeaderHelper } from 'app/presentation/navigation/header/HeaderHelper';
import { Routes } from 'app/presentation/navigation/Routes';
import { appString, getString } from 'app/presentation/theme/Theme';

export const createMoreStack = () => {
  const Stack = createStackNavigator();
  const strMore = getString(appString.more).toUpperCase();
  return (
    <Stack.Navigator initialRouteName={Routes.Main.More}>
      <Stack.Screen name={Routes.Main.More} component={AppScreens.Main.More}
                    options={HeaderHelper.makeMainScreenHeader(strMore)}/>
    </Stack.Navigator>
  );
};