import React  from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { HeaderHelper } from 'app/presentation/navigation/header/HeaderHelper';
import { Routes } from 'app/presentation/navigation/Routes';
import { appString, getString } from 'app/presentation/theme/Theme';

export const createMarketStack = () => {
  const Stack = createStackNavigator();
  const strMarket = getString(appString.markets).toUpperCase();
  return (
    <Stack.Navigator initialRouteName={Routes.Main.Markets}>
      <Stack.Screen name={Routes.Main.Markets} component={AppScreens.Main.Markets}
                    options={HeaderHelper.makeMainScreenHeader(strMarket)}/>
    </Stack.Navigator>
  );
};