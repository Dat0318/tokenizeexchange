import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { forSlide } from 'app/presentation/navigation/animation';
import { defaultHeaderOptions, defaultHeaderStyle } from 'app/presentation/navigation/header/HeaderHelper';
import { appColors } from 'app/presentation/theme/Theme';
import { Routes } from 'app/presentation/navigation/Routes';
import { AppScreens } from 'app/presentation/navigation/AppScreens';

export const createAuthStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName={Routes.Auth.SignIn}
      screenOptions={{
        // ...defaultHeaderOptions,
        // headerTitleAlign: 'center',
        // cardStyleInterpolator: forSlide,
        // headerStyle: {...defaultHeaderStyle}
      }}>
      <Stack.Screen name={Routes.Auth.SignIn} component={AppScreens.Auth.SignIn}
                    options={{headerShown: false }}/>
    </Stack.Navigator>
  );
};

