import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { HeaderHelper } from 'app/presentation/navigation/header/HeaderHelper';
import { Routes } from 'app/presentation/navigation/Routes';
import { appString, getString } from 'app/presentation/theme/Theme';

export const createPortfolioStack = () => {
  const Stack = createStackNavigator();
  const strPortfolio = getString(appString.portfolio).toUpperCase();
  return (
    <Stack.Navigator initialRouteName={Routes.Main.Portfolio}>
      <Stack.Screen name={Routes.Main.Portfolio} component={AppScreens.Main.Portfolio}
                    options={HeaderHelper.makeMainScreenHeader(strPortfolio)}/>
    </Stack.Navigator>
  );
};