import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { HeaderHelper } from 'app/presentation/navigation/header/HeaderHelper';
import { Routes } from 'app/presentation/navigation/Routes';
import { appString, getString } from 'app/presentation/theme/Theme';

export const createWalletsStack = () => {
  const Stack = createStackNavigator();
  const strWallets = getString(appString.wallets).toUpperCase();

  return (
    <Stack.Navigator initialRouteName={Routes.Main.Wallets}>
      <Stack.Screen name={Routes.Main.Wallets} component={AppScreens.Main.Wallets}
                    options={HeaderHelper.makeMainScreenHeader(strWallets)}/>
    </Stack.Navigator>
  );
};