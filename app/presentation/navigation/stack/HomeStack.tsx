import React  from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { AppScreens } from 'app/presentation/navigation/AppScreens';
import { HeaderHelper } from 'app/presentation/navigation/header/HeaderHelper';
import { Routes } from 'app/presentation/navigation/Routes';
import { appString, getString } from 'app/presentation/theme/Theme';

export const createHomeStack = () => {
  const Stack = createStackNavigator();
  const strHome = getString(appString.home).toUpperCase();
  return (
    <Stack.Navigator initialRouteName={Routes.Main.Home}>
      <Stack.Screen name={Routes.Main.Home} component={AppScreens.Main.Home}
                    options={HeaderHelper.makeMainScreenHeader(strHome)}/>
    </Stack.Navigator>
  );
};