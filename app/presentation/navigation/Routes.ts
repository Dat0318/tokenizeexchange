export const Routes = {
  Root: {
    Start: 'Root.Start',
    AuthStack: 'Root.AuthStack',
    Main: 'Root.Main',
  },
  Auth: {
    SignIn: 'Auth.SignIn',
  },
  Main: {
    Home: 'Main.Home',
    Markets: 'Main.Markets',
    Wallets: 'Main.Wallets',
    Portfolio: 'Main.Portfolio',
    More: 'Main.More',
  },
};
