import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Routes } from 'app/presentation/navigation/Routes';
import { AppStack } from 'app/presentation/navigation/stack';
import { appColors, appDimensions, appImage, appString, getString } from 'app/presentation/theme/Theme';

const ImageHome = appImage.Icons.HomeSVG;
const ImageActiveHome = appImage.Icons.ActiveHomeSVG;
const ImageMarkets = appImage.Icons.MarketsSVG;
const ImageActiveMarkets = appImage.Icons.ActiveMarketsSVG;
const ImageWallets = appImage.Icons.WalletsSVG;
const ImageActiveWallets = appImage.Icons.ActiveWalletsSVG;
const ImagePortfolio = appImage.Icons.PortfolioSVG;
const ImageActivePortfolio = appImage.Icons.ActivePortfolioSVG;
const ImageMore = appImage.Icons.MoreSVG;
const ImageActiveMore = appImage.Icons.ActiveMoreSVG;
let Icon = null;

const getIcon = (route: any, focused: boolean) => {
  switch (route.name) {
    case Routes.Main.Home:
      Icon = focused ? ImageActiveHome : ImageHome;
      break;
    case Routes.Main.Markets:
      Icon = focused ? ImageActiveMarkets : ImageMarkets;
      break;
    case Routes.Main.Wallets:
      Icon = focused ? ImageActiveWallets : ImageWallets;
      break;
    case Routes.Main.Portfolio:
      Icon = focused ? ImageActivePortfolio : ImagePortfolio;
      break;
    case Routes.Main.More:
      Icon = focused ? ImageActiveMore : ImageMore;
      break;
    default:
      Icon = ImageHome;
      break;
  }
  return Icon;
};

export const createBottomTabs = () => {
  const Tab = createBottomTabNavigator();
  const strHome = getString(appString.home);
  const strMarkets = getString(appString.markets);
  const strWallets = getString(appString.wallets);
  const strPortfolio = getString(appString.portfolio);
  const strMore = getString(appString.more);
  return (
    <Tab.Navigator
      initialRouteName={Routes.Main.Home}
      screenOptions={({route}: { route: any }) => ({
        tabBarIcon: ({focused}: { focused: boolean }) => {
          const Icon = getIcon(route, focused);
          return <Icon height={28}/>;
        },
      })}
      tabBarOptions={{
        labelStyle: {
          fontSize: appDimensions.Font.Small,
          fontWeight: '500',
        },
        activeTintColor: appColors.Hex.Hex6081FA,
        inactiveTintColor: appColors.Hex.Hex9194BB,
      }}
    >
      <Tab.Screen name={Routes.Main.Home} component={AppStack.createHomeStack}
                  options={{tabBarLabel: strHome}}/>
      <Tab.Screen name={Routes.Main.Markets} component={AppStack.createMarketStack}
                  options={{tabBarLabel: strMarkets}}/>
      <Tab.Screen name={Routes.Main.Wallets} component={AppStack.createWalletsStack}
                  options={{tabBarLabel: strWallets}}/>
      <Tab.Screen name={Routes.Main.Portfolio} component={AppStack.createPortfolioStack}
                  options={{tabBarLabel: strPortfolio}}/>
      <Tab.Screen name={Routes.Main.More} component={AppStack.createMoreStack}
                  options={{tabBarLabel: strMore}}/>
    </Tab.Navigator>
  );
};