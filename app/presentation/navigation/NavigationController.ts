import { CommonActions, NavigationContainerRef, StackActions } from '@react-navigation/native';
import React from 'react';

const navigationRef = React.createRef<NavigationContainerRef>();

const navigate = (name: string, params?: any) => {
  navigationRef.current?.navigate(name, params);
};

const push = (name: string, params?: any) => {
  navigationRef.current?.dispatch(StackActions.push(name, params));
};

const goBack = () => {
  navigationRef.current?.goBack();
};

const popToTop = () => {
  navigationRef.current?.dispatch(StackActions.popToTop());
};

const navigateWithReset = (name: string) => {
  navigationRef.current?.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{name: name}],
    }),
  );
};

export const NavigationController = {
  navigationRef,
  navigate,
  push,
  goBack,
  navigateWithReset,
  popToTop
};
