import StartScreen from 'app/presentation/module/start/StartScreen';
import HomeScreen from 'app/presentation/module/home/HomeScreen';
import MarketsScreen from 'app/presentation/module/market/MarketsScreen';
import WalletsScreen from 'app/presentation/module/wallets/WalletsScreen';
import PortfolioScreen from 'app/presentation/module/portfolio/PortfolioScreen';
import MoreScreen from 'app/presentation/module/more/MoreScreen';
import SignInScreen from 'app/presentation/module/authen/SignIn';

export const AppScreens = {
  Auth: {
    SignIn: SignInScreen
  },
  Main: {
    Start: StartScreen,
    Home: HomeScreen,
    Markets: MarketsScreen,
    Wallets: WalletsScreen,
    Portfolio: PortfolioScreen,
    More: MoreScreen,
  },
}