import React, { useEffect, useRef, useState } from 'react';
import { Platform, View } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { moderateScale } from 'react-native-size-matters';
import styled from 'styled-components/native';
import _ from 'lodash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { appColors, appDimensions, appImage, appString, getString } from 'app/presentation/theme/Theme';
import { ViewContainer1 } from 'app/presentation/component/view';
import { PasswordInput, TextInput } from 'app/presentation/component/input';
import { SolidButton } from 'app/presentation/component/button';
import { getWindowWidth } from 'app/common/helper/WindowSizeHelper';
import { TextPrimary } from 'app/presentation/component/text';
import { Logo } from 'app/presentation/component/logo';
import { usePrevious } from 'app/common/helper/HookHelper';
import { MainNavigator } from 'app/presentation/navigation/navigator/MainNavigator';

import { signIn } from 'app/data/redux/customer/CustomerAction';
import { IState as IReducerState } from 'app/data/redux/common';

import CustomerSelector from 'app/data/redux/customer/CustomerSelector';
import DropDownHolder from 'app/common/helper/DropDownHolder';
import CheckboxNormal from 'app/presentation/component/checkbox';
import KeychainHelper from 'app/common/helper/KeychainHelper';
import { CommonRepository } from 'app/data/repositories/CommonRepository';

const TEXT_INPUT_WIDTH = getWindowWidth() - appDimensions.Spacing.MediumX * 2;
const ImageCheckbox = appImage.Icons.CheckboxSVG;

const SignInScreen = (props: any) => {
  const dispatch = useDispatch();

  const signInState: IReducerState<any> = useSelector(
    state => CustomerSelector.selectSignIn(state)
  );

  const inputUsername = useRef<any>();
  const inputPassword = useRef<any>();
  const [userError, setUserError] = useState('');
  const [userValue, setUserValue] = useState(null);

  const [pwdError, setPwdError] = useState('');
  const [pwdValue, setPwdValue] = useState(null);
  const [isChecked, setCheckbox] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const strAccount = getString(appString.email);
  const strPassword = getString(appString.password);
  const strSignIn = getString(appString.signIn).toUpperCase();
  const strSignIn1 = getString(appString.signInNormal);
  const strPlzSignInContinue = getString(appString.plzSignInContinue);
  const strRememberMe = getString(appString.rememberMe);
  const strForgotPassword = getString(appString.forgotPassword);
  const strSignUp = getString(appString.signUp);

  useEffect(() => {
    KeychainHelper.getCredentials().then((resp: any) => {
      let userName = resp.username;
      setUserValue(userName);
    });
    checkRememberMe();
  }, []);

  useEffect(() => {
    CommonRepository.getRememberMe().then((resp: any) => {
      let isRemember = (resp === 'true');
      setCheckbox(isRemember);
    });
  });

  const signInPrevState = usePrevious<IReducerState<any>>(signInState);
  useEffect(() => {
    if (!signInPrevState || signInState.actionType === signInPrevState?.actionType) {
      return;
    }
    if (signInState.success) {
      if (isChecked) {
        KeychainHelper.setCredentials(userValue, pwdValue).then();
      }
      DropDownHolder.showSuccessAlert(getString(appString.signInSuccess));
      MainNavigator.openMain();
      setIsLoading(false);
    } else if (signInState.error) {
      DropDownHolder.showErrorAlert(signInState.error.message);
      setIsLoading(false);
    }
  }, [signInState]);

  const validateForm = () => {
    let isError = false;
    if (null === userValue || _.isEmpty(userValue)) {
      DropDownHolder.showErrorAlert(getString(appString.missingInput));
      isError = true;
    } else setUserError('');

    if (null === pwdValue || _.isEmpty(pwdValue)) {
      DropDownHolder.showErrorAlert(getString(appString.missingInput));
      isError = true;
    } else setPwdError('');
    return isError;
  };

  const checkRememberMe = () => {
    CommonRepository.getRememberMe().then((resp: any) => {
      let isRemember = (resp === 'true');
      if (!isRemember) {
        setUserValue(null);
      }
    });
  };

  const onPressLogin = () => {
    if (validateForm()) {
      return;
    }
    setIsLoading(true);
    dispatch(signIn({ email: userValue, password: pwdValue }));
  };

  const callBackCheckBox = () => {
    setCheckbox(!isChecked);
    CommonRepository.saveRememberMe(!isChecked).then();
  };

  return (
    <ViewContainer1>
      <ViewImageBackgroundContainer source={appImage.Bkg.BkgPNG}>
        <ViewContainer>
          <Logo marginTop={moderateScale(55)}/>
          <TextSignIn>{strSignIn1}</TextSignIn>
          <TextPlzSignInContinue>{strPlzSignInContinue}</TextPlzSignInContinue>
          <TextInput
            containerStyle={{
              borderWidth: 2, borderColor: '#75a2ff',
              backgroundColor: appColors.TextInput,
              marginLeft: appDimensions.Spacing.MediumX,
              width: TEXT_INPUT_WIDTH,
            }}
            inputStyle={{
              fontSize: appDimensions.Font.Large,
              fontFamily: 'Roboto-Medium'
            }}
            forwardRef={inputUsername}
            placeholder={strAccount}
            labelEnabled={true}
            returnKeyType="done"
            blurOnSubmit={false}
            autoFocus={false}
            onSubmitEditing={() => inputPassword?.current?.focus()}
            errorMessage={userError}
            value={userValue}
            onTextChanged={(text: any) => {
              setUserValue(text);
            }}
          />
          <PasswordInput
            containerStyle={{
              borderWidth: 2, borderColor: '#75a2ff',
              backgroundColor: appColors.TextInput,
              marginTop: appDimensions.Spacing.MediumX,
              marginLeft: appDimensions.Spacing.MediumX,
              width: TEXT_INPUT_WIDTH,
            }}
            inputStyle={{
              fontSize: appDimensions.Font.Large,
              fontFamily: 'Roboto-Medium'
            }}
            forwardRef={inputPassword}
            placeholder={strPassword}
            labelEnabled={true}
            returnKeyType="done"
            onSubmitEditing={onPressLogin}
            errorMessage={pwdError}
            onTextChanged={(text: any) => {
              setPwdValue(text);
            }}
          />

          <ViewRow>
            <View style={{ flexDirection: 'row' }}>
              <CheckboxNormal
                title={strRememberMe}
                isSelected={isChecked}
                iconRight
                onChecked={callBackCheckBox}/>
            </View>
            <ViewAbsolute>
              <TextForgotPass>{strForgotPassword}</TextForgotPass>
            </ViewAbsolute>
          </ViewRow>

          <SolidButton
            title={strSignIn}
            loading={isLoading}
            buttonStyle={{
              height: moderateScale(55),
            }}
            containerStyle={{
              marginHorizontal: appDimensions.Spacing.Large,
              marginTop: moderateScale(45)
            }}
            titleStyle={{
              color: appColors.Hex.Hex6081FA,
              fontWeight: 'bold',
              fontSize: appDimensions.Font.MediumX
            }}
            onPress={onPressLogin}/>
          <TextSignUp>{strSignUp}</TextSignUp>
        </ViewContainer>
      </ViewImageBackgroundContainer>
    </ViewContainer1>
  );
};
export default SignInScreen;

const ViewContainer = styled(KeyboardAwareScrollView)`
  flex: 1;
  margin-top: ${Platform.OS === 'ios' ? moderateScale(50) : 0};
`;

const ViewImageBackgroundContainer = styled.ImageBackground`
  width: ${'100%'};
  height: ${'100%'};
  position: absolute;
  top: 0;
`;

const TextSignIn = styled(TextPrimary)`
  padding-top: ${appDimensions.Spacing.HugeXX};
  padding-bottom: ${appDimensions.Spacing.Small};
  padding-left: ${appDimensions.Spacing.HugeX};
  padding-right: ${appDimensions.Spacing.HugeX};
  text-align: center;
  font-size: ${appDimensions.Font.HugeX};
  font-weight: ${'400'};
  color: ${appColors.White};
`;

const TextPlzSignInContinue = styled(TextSignIn)`
  padding-top: ${appDimensions.Spacing.Small};
  padding-bottom: ${moderateScale(70)};
  font-size: ${appDimensions.Font.Large};
`;

const TextSignUp = styled(TextPrimary)`
  padding-top: ${appDimensions.Spacing.Medium};
  margin-bottom: ${appDimensions.Spacing.Huge};
  text-align: center;
  font-size: ${appDimensions.Font.Large};
  font-weight: ${'400'};
  color: ${appColors.White};
`;

const TextForgotPass = styled(TextPrimary)`
  padding-top: ${appDimensions.Spacing.Small};
  padding-bottom: ${moderateScale(70)};
  font-size: ${appDimensions.Font.Large};
  font-weight: ${'400'};
  color: ${appColors.White};
`;

const ViewRow = styled.View`
  flex-direction: row;
`;

const ViewAbsolute = styled.View`
  position: absolute;
  right: ${appDimensions.Spacing.MediumX};
`;