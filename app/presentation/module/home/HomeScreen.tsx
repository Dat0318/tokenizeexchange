import React, { useEffect }  from 'react';


import { useDispatch } from 'react-redux';
import styled from 'styled-components/native';

import { appColors } from 'app/presentation/theme/Theme';
import { getBasedMarkets, getMarkets, getSummaries } from 'app/data/redux/home/HomeAction';

const HomeScreen = (props: any) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBasedMarkets());
    dispatch(getMarkets());
    dispatch(getSummaries());
  }, []);
  return null;
}
export default HomeScreen;

const ViewContainer = styled.View`
  flex: 1;
  backgroundColor: ${appColors.White};
`;