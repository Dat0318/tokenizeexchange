import React from 'react';
import { Animated, Easing } from 'react-native';

import styled from 'styled-components/native';

import { appColors, appImage } from 'app/presentation/theme/Theme';
import { MainNavigator } from 'app/presentation/navigation/navigator/MainNavigator';

const IconLogo = appImage.Logo.LogoSVG;
const StartScreen = () => {
  let scaleValue = new Animated.Value(0.2);
  let opacityValue = new Animated.Value(1);

  Animated.parallel([
    Animated.timing(scaleValue, {
      toValue: 1,
      easing: Easing.linear,
      duration: 1000,
      useNativeDriver: true
    }),
    Animated.timing(opacityValue, {
      toValue: 0,
      easing: Easing.linear,
      duration: 1000,
      useNativeDriver: true
    })
  ]).start(() => {
    handleNavigation();
  });

  const handleNavigation = () => {
    MainNavigator.openAuthStackWithWalkThrough();
  };

  return (
    <ViewContainer>
      <ViewImageBackgroundContainer source={appImage.Bkg.BkgPNG}>
        <Animated.View
          style={{
            transform: [
              { scale: scaleValue }
            ],
            opacity: opacityValue,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <IconLogo height={500} width={500}/>
        </Animated.View>
      </ViewImageBackgroundContainer>
    </ViewContainer>
  );
};
export default StartScreen;

const ViewContainer = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${appColors.Backdrop};
`;

const ViewImageBackgroundContainer = styled.ImageBackground`
  width: ${'100%'};
  height: ${'100%'};
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
`;