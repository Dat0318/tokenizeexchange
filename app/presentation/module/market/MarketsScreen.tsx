import React, { useCallback, useEffect, useState } from 'react';
import { FlatList, RefreshControl } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';

import { appColors, appDimensions } from 'app/presentation/theme/Theme';
import { IState as IReducerState } from 'app/data/redux/common';

import { getMarkets, getSummaries } from 'app/data/redux/home/HomeAction';

import HomeSelector from 'app/data/redux/home/HomeSelector';
import MarketsItem from 'app/presentation/module/market/marketItem/MarketItem';
import TabBasedMarket from 'app/presentation/module/market/baseMarketItem/BaseMarketItem';

const MarketsScreen = (props: any) => {
    const dispatch = useDispatch();
    const getMarketsState: IReducerState<any> = useSelector(
      state => HomeSelector.selectGetMarkets(state)
    );
    const getSummariesState: IReducerState<any> = useSelector(
      state => HomeSelector.selectGetSummaries(state)
    );

    const [refreshing, setRefreshing] = React.useState(false);
    const [focusedItem, setFocusedItem] = useState('');
    const [marketItems, setMarketItem] = useState<any>(getMarketsState?.data?.data?.[0]);
    const combineStateProps = { ...props, marketItems, focusedItem, refreshing };

    useEffect(() => {
      setFocusedItem(getMarketsState?.data?.data?.[0]?.title);
    }, []);

    const keyExtractor = useCallback(item => item.coin, []);

    const onRefresh = useCallback(() => {
      dispatch(getMarkets());
      dispatch(getSummaries());
    }, [refreshing]);

    const renderEmpty = () => {
      return null;
    };

    const renderItem = ({ item, index }: { item: any, index: number }) => {
      return <MarketsItem marketItem={item}
                          summaries={getSummariesState?.data?.data || []}
                          key={item?.marketName || index.toString()}/>;
    };

    const _onPressBasedMarkets = (item: any) => {
      if (focusedItem === `${item.title}`) {
        setFocusedItem('');
        setMarketItem(item);
      } else {
        setFocusedItem(`${item.title}`);
        setMarketItem(item);
      }
    };

    const renderItemBasedMarkets = ({ item, index }: { item: any, index: number }) => {
      return (
        <TabBasedMarket
          item={item}
          key={item?.title || index.toString()}
          focusedItem={focusedItem}
          onPress={_onPressBasedMarkets}/>
      );
    };

    // console.log('getMarketsState:', getMarketsState);
    // console.log('marketItems_getMarketsState:', getMarketsState?.data?.data);
    // console.log('marketItems:', marketItems);

    return (
      <ViewContainer>
        <ViewBasedMarkets>
          <FlatList data={getMarketsState?.data?.data || []}
                    initialNumToRender={10}
                    renderItem={renderItemBasedMarkets}
                    horizontal={true}
                    keyExtractor={keyExtractor}
                    ListEmptyComponent={renderEmpty}/>
        </ViewBasedMarkets>
        <FlatList data={marketItems?.list || []}
                  initialNumToRender={10}
                  extraData={combineStateProps}
                  renderItem={renderItem}
                  keyExtractor={keyExtractor}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={onRefresh}/>
                  }
                  ListEmptyComponent={renderEmpty}/>
      </ViewContainer>
    );
  }
;

export default React.memo(MarketsScreen);

const ViewContainer = styled.View`
  flex: 1;
`;

const ViewBasedMarkets = styled.View.attrs((props: any) => ({
  marginTop: props.marginTop ? props.marginTop : appDimensions.Spacing.SmallX
}))`
  height: ${36};
  background-color: ${appColors.Black};
  margin-top: ${props => props.marginTop};
  justify-content: center;
  align-items: center;
`;