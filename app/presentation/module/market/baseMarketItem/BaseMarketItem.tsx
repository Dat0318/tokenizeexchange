import React from 'react';

import styled from 'styled-components/native';

import { TextPrimary } from 'app/presentation/component/text';
import { appColors, appDimensions } from 'app/presentation/theme/Theme';

interface IProps {
  item: any;
  onPress: (item: any) => void;
  focusedItem: string;
}

const TabBasedMarket = (props: IProps) => {
  const { item, onPress, focusedItem } = props;
  const isFocused = (focusedItem === `${item.title}`);

  const _onPressBasedMarketItem = () => {
    if (onPress) {
      onPress(item);
    }
  };
  return (
    <ButtonTitleNameCategory backgroundColor={isFocused ? appColors.White : appColors.Transparent}
                             onPress={_onPressBasedMarketItem}>
      <TextNameCategory onPress={_onPressBasedMarketItem}
                        color={isFocused ? appColors.Black : appColors.White}>
        {(item.title)?.toUpperCase()}</TextNameCategory>
    </ButtonTitleNameCategory>
  );
};

export default React.memo(TabBasedMarket);

const TextNameCategory = styled(TextPrimary).attrs((props: any) => ({
  color: props.color !== undefined ? props.color : appColors.White
}))`
  color: ${props => props.color};
  text-align: center;
  textAlignVertical: center;
  font-weight: 700;
  font-size: ${appDimensions.Font.Small};
  paddingHorizontal: ${8};
`;

const ButtonTitleNameCategory = styled.TouchableOpacity.attrs((props: any) => ({
  backgroundColor: props.backgroundColor ? props.backgroundColor : appColors.Transparent
}))`
  width: auto;
  background-color: ${props => props.backgroundColor};
  justify-content: center;
  align-items: center;
  margin-right: ${12}
`;