import React from 'react';
import { View } from 'react-native';

import styled from 'styled-components/native';

import { appColors, appDimensions } from 'app/presentation/theme/Theme';
import { TextPrimary } from 'app/presentation/component/text';
import { getWindowWidth } from 'app/common/helper/WindowSizeHelper';

import getIconCoin from 'app/presentation/component/iconCoin';

const ITEM_WIDTH = getWindowWidth() - appDimensions.Spacing.MediumX * 2;
const CONTENT_WIDTH = ITEM_WIDTH - (45 + 28 + 28);


const MarketsItem = (props: any) => {
  const { marketItem, summaries } = props;
  let marketName = `${marketItem?.baseCurrency}-${marketItem?.marketCurrency}`;
  const findSummaryByMarketName = summaries.find((item: any) => {
    return item.market === marketName;
  });
  let lastPrice = findSummaryByMarketName?.lastPrice || 0;
  let percent = (findSummaryByMarketName?.lastPrice * 100) / (findSummaryByMarketName?.prevPrice);

  console.log('marketItem:', marketItem.marketCurrency);
  const IconCoin = getIconCoin(marketItem?.marketCurrency);
  const nameCodeCoin = marketItem?.marketCurrency || '';
  const priceCoin = `${'$'}${lastPrice}`;
  const nameDetailCoin = marketItem?.marketCurrencyLong || '';
  const percentCoin = percent >= 100 ? `+${(percent - 100).toFixed(2)}` : `-${(100 - percent).toFixed(2)}`;
  const percentColor = percent >= 100 ? 'green' : 'red';

  return (
    <ViewItem>
      <ViewWrapIcon>
        <IconCoin width={45} height={45} style={{
          marginHorizontal: appDimensions.Spacing.MediumX * 2
        }}/>
      </ViewWrapIcon>
      <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
        <View style={{ flexDirection: 'row', width: CONTENT_WIDTH }}>
          <TextNameCoin>{nameCodeCoin}</TextNameCoin>
          <ViewAbsolute>
            <TextNameCoin>{priceCoin}</TextNameCoin>
          </ViewAbsolute>
        </View>
        <View style={{ flexDirection: 'row', width: CONTENT_WIDTH }}>
          <TextNameDescription>{nameDetailCoin}</TextNameDescription>
          <ViewAbsolute>
            <TextPercentCoin
              color={percentColor}>{`${percentCoin}${'\n %'}`}</TextPercentCoin>
          </ViewAbsolute>
        </View>
      </View>
    </ViewItem>
  );
};

export default React.memo(MarketsItem);

const ViewItem = styled.View`
  flex-direction: row;
  width: ${ITEM_WIDTH};
  height: ${90};
  margin-left: ${appDimensions.Spacing.MediumX};
  border-radius: ${appDimensions.Spacing.Medium};
  background-color: ${appColors.White};
  margin-top: ${appDimensions.Spacing.Small};
`;

const TextNameDescription = styled(TextPrimary)`
  text-align: center;
  font-size: ${appDimensions.Font.Large};
  font-weight: ${'400'};
  color: ${appColors.Gray.C800};
`;

const TextNameCoin = styled(TextPrimary)`
  text-align: center;
  font-size: ${appDimensions.Font.Large};
  font-weight: ${'400'};
  color: ${appColors.Black};
`;

const TextPercentCoin = styled(TextPrimary).attrs((props: any) => ({
  color: props.color || appColors.Black,
}))`
  text-align: center;
  font-size: ${appDimensions.Font.Large};
  font-weight: ${'400'};
  color: ${props => props.color};
`;

const ViewAbsolute = styled.View`
  position: absolute;
  right: ${0};
`;

const ViewWrapIcon = styled.View`
  width: ${73};
  justify-content: center;
  align-items: center;
`;