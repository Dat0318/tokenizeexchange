import 'react-native-gesture-handler';
import React, { useEffect } from 'react';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { configureStore } from 'app/data/redux/Store';
import { configureLocalization, createAppTheme } from 'app/presentation/theme/Theme';

import RootContainer from 'app/presentation/RootContainer';

const store = configureStore();

const App = () => {
  console.disableYellowBox = true;
  useEffect(() => {
    configureLocalization();
  }, []);

  return (
    <ThemeProvider theme={createAppTheme}>
      <Provider store={store}>
        <SafeAreaProvider>
          <RootContainer/>
        </SafeAreaProvider>
      </Provider>
    </ThemeProvider>
  );
};
export default App;
